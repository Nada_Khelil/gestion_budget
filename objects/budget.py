#-*- coding:utf-8 -*-

from openerp.osv import osv,fields
from openerp.report import report_sxw
import openerp.addons.decimal_precision as dp



class budget_budget(osv.osv):
    
    _inherit = "crossovered.budget.lines"

    
    def _perc(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            if line.planned_amount <> 0.00:
                res[line.id] = float((line.practical_amount or 0.0) / line.planned_amount) * 100
            else:
                res[line.id] = 0.00
        return res
    
    def _ecart(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            if line.planned_amount <> 0.00:
                res[line.id] = (line.planned_amount - line.practical_amount) 
            else:
                res[line.id] = 0.00
        return res
    
    _columns ={
    'percentage':fields.function(_perc, string='Percentage', type='float'),
    'ecart':fields.function(_ecart, string='Ecart',digits_compute=dp.get_precision('Account'), type='float'),



               }
    
budget_budget()


   