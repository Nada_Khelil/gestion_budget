{
    "name": "ARSAT BUDGET",
    "version": "1.0",
    "depends": ["base",'account_voucher', 'account', 'account_budget','precision_decimale_tchad'],
    "author": "RCA",
    "category": "account",
    "description": """
    Ce module permet la gestion budgetaire 
    """,
    "init_xml": [],
    'data': [
            "report/report_budget.xml",
            "report/report_crossoveredbudget.xml",
            "report/report_analyticaccountbudget.xml",
            "views/budget_view.xml",
            ],
           
    'demo_xml': [],
    'installable': True,
    'active': False,
}